# Bash aliases #

A list of useful bash aliases.

## Install ##

1. `git clone git@github.com:DevCreel/Bash-Aliases.git ~/BashAliases`
2. `cd && sh BashAliases/install.sh`